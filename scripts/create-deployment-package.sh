#!/bin/bash

set -e

cd web-app

build_number=${1}

RAILS_ENV=production rake assets:precompile

echo ${build_number} > VERSION

tar -czf ${HOME}/artifacts/tracker-${build_number}.tar.gz . \
--exclude=features \
--exclude=test \
--exclude=scripts \
--exclude=.git \
--exclude=.idea