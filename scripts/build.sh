#!/bin/bash

set -e

cd web-app

CURRENT_PATH=$(pwd)

ln -sf ${CURRENT_PATH}/config/database.yml.ci ${CURRENT_PATH}/config/database.yml

rake db:migrate RAILS_ENV=test
bundle exec rake
bundle exec cucumber

