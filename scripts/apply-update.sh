#!/bin/bash

set -e

if [ "$(id -u)" != "0" ]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi

RBENV_PATH=${HOME}/.rbenv
RBENV_BIN_PATH=${RBENV_PATH}/bin
RBENV_SHIMS_PATH=${RBENV_PATH}/shims
MYSQL_DB_NAME="webapp_production"
MYSQL_USER_NAME="root"

PATH=${RBENV_BIN_PATH}:${RBENV_SHIMS_PATH}:$PATH

project_root=${HOME}/todo-list-app
instance_name=${1}
package_name=${2}
