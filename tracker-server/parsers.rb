require_relative 'factories'

class TrackerParser
  attr_accessor :string
  attr_accessor :row_db

  def parse
    #   100% будет переопределен
  end


end

class Coban103Parser < TrackerParser
  def parse(message)

    info = self.cut_message(message)

    #   Переопределяем правила распарсивания для конкретного парсера
    @message_builder = MessageBuilder.new
    # логика жпс_103
    @message_builder.add_type info[0]
    @message_builder.add_time info[1]
    @message_builder.add_sim info[2]
    # @message_builder.add_tracker  ????

    if info.count == 5
      @message_builder.add_latitude info[3]
      @message_builder.add_longtitute info[4]
    end


    @message_builder.build
  end

  def cut_message(message)
    arr_string = message.split(',')
    if arr_string.count < 4
      # трекер подает признаки жизни. игнорируем их
      return InvalidParseString
    end

    if arr_string.count > 12
      @cut_message = arr_string[0,10]
      @cut_message.shift #удалили имей, он нам известен
      @cut_message.delete_at(3) # удалили маркер, отвечающий за валидность передачи данных
      @cut_message.delete_at(3) # удалили точку отсчета координат. да покойся ты с миром
      @cut_message.delete_at(3) # удалили навигационный маркер, и без него справимся
      @cut_message.delete_at(4) # еще один навигационный маркер нас покидает
      return @cut_message
# вот так на много лучше. 0 - экшен, 1 - юникстайм, 2 - симкарта, 3 - координата, 4 - координата
    end

    if arr_string.count == 5 || arr_string[4] == "L"
      # #   cлучай, когда строка валидна, но GPS сигнала нет(нет координат)
      @cut_message = arr_string[0,4]
      @cut_message.shift #удалили имей, он нам известен
      return @cut_message # 0 - экшн, 1 - время,  2 - сим
    end
  end

end







