require 'json'

class TrackerMessage
  attr_accessor :type, :latitude, :longitude
  def initialize
    @type = nil
    @latitude = nil
    @longitude = nil
  end

  def to_json()
    hash = {}
    self.instance_variables.each do |var|
      hash[var] = self.instance_variable_get var
    end
    p hash
    return hash.to_json
  end

end