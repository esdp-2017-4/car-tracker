require 'date'
require_relative 'utils'
require_relative 'factories'
require_relative 'parsers'
require 'dat-tcp'

# TODO валидировать на корректные данные

class Worker

  include DatTCP::Worker

  def work!(socket)

    while @message = socket.gets
      # @message = socket.read
      if @message.chomp == ""
        socket.puts 'Сообщение пустое'
      else
        puts "Client : #{@message.chomp}"



        message_handle = params[:message_handler_factory].create_message_handler(@message)

        message_handle.handle

        socket.puts "Данные принял"

      end
      break if @message =~ /^\s*$/
    end
    socket.write('Связь прервана!')
  ensure
    socket.close
  end
end


class Application

  def self.create_application
    application = Application.new
    application.init
    return application
  end

  def start
    @server.listen('0.0.0.0', 2000)
    @server.start.join
  end

  def init
    create_message_handler_dependencies
    create_message_handler_factory
    create_server
  end

  private

  def create_message_builder_factory
    @message_builder_factory = MessageBuilderFactory.new
  end

  def create_parser_factory
    @parser_factory = ParserFactory.new
  end

  def create_saver_factory
    @saver_factory = SaverFactory.new
  end

  def create_tracker_finder_factory
    @tracker_finder_factory = TrackerFinderFactory.new
  end


  def create_message_handler_factory

    @message_handler_factory = MessageHandlerFactory.new(
        @message_builder_factory,
        @parser_factory,
        @saver_factory,
        @tracker_finder_factory

    )
  end

  def create_server
    @server = DatTCP::Server.new(Worker, :worker_params => { :message_handler_factory => @message_handler_factory } )
  end

  def create_message_handler_dependencies
    create_message_builder_factory
    create_parser_factory
    create_saver_factory
    create_tracker_finder_factory
  end



end

application = Application.create_application
application.start

p 777