class TrackerNotFoundError < StandardError

end

class InvalidMessageString < StandardError

end

class InvalidParseString < StandardError

end
