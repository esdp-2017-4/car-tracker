require 'test/unit'
require_relative 'utils'
require_relative 'exeptions'

class TrackerFinderTests < Test::Unit::TestCase

  def test_should_find_tracker_with_valid_string
    test_message = 'imei:359586015829802,move,0809231429,13554900601,F,062947.294,A,2234.4026,N,11354.3277,E,0.00,;'
    tracker_finder = TrackerFinder.new(test_message)
    expected_tracker = "coban_103"
    actual_tracker = tracker_finder.find_tracker

    assert_equal(expected_tracker, actual_tracker)
  end

  def test_should_raise_excetion_with_invalid_string
    test_message = 'hsdjfkglfh;'

    assert_raise("InvalidMessageString") {
      tracker_finder = TrackerFinder.new(test_message)
    }
  end

end


# class Coban103ParserTests < Test::Unit::TestCase
#   def test_should_return_valid_object_with_valid_string
#     test_message = 'imei:359586015829802,move,0809231429,13554900601,F,062947.294,A,2234.4026,N,11354.3277,E,0.00,;'
#     tracker_finder = TrackerFinder.new(test_message)
#     actual_tracker = tracker_finder.find_tracker
#     actual_parser = parser_factory.create_parser(actual_tracker)
#     actual_message = parser.parse(test_message)
#     # expected_message = "Message:0x007f8b4887dc20 @type="move", @tracker=nil, @time=1995-08-24 08:30:29 +0600, @sim="13554900601", @latitude="2234.4026", @longtitute="11354.3277"
#
#     assert_equal(expected_message, actual_message)
#
#   end
# end