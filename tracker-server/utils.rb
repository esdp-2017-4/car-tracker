require 'net/http'
require 'json'

class TrackerFinder
  attr_reader :imei
  def initialize(message)
    begin
      @message = message
      matched = message.match(/(?<imei>\d{15,16})/)
      @imei = matched["imei"]
    rescue NoMethodError
      raise InvalidMessageString
    end
  end

  def find_tracker
    # Делаешь запрос на сервер если не найден то кидаешь эксепшн иначе отдаешь тип трекера
    # Пытаемся провалдировать через апи
    # if response.status_code == 404
    #   raise TrackerNotFoundError
    # end
    'coban_103'
    # TODO апи для получения трекера
  end

end

class Message
  attr_accessor :type, :tracker, :time, :sim, :latitude, :longtitute

  def as_json(options={})
    {
        type: @type,
        tracker: @tracker,
        time: @time,
        sim: @sim,
        latitude: @latitude,
        longtitute: @longtitute
    }
  end

  def to_json(*options)
    as_json(*options).to_json(*options)
  end
end


class MessageSaver

  attr_accessor :message, :imei

  def initialize(message, imei)
    @message = message
    @imei = imei
  end

  def save()
    uri = URI("http://localhost:3000/api_for_parser/#{@imei}/save_message")
    http = Net::HTTP.new(uri.host, uri.port)
    req = Net::HTTP::Post.new(uri.path, 'Content-Type' => 'application/json')

    message_json = @message.to_json
    p message_json
    req.body = message_json

    res = http.request(req)
  end
end


class MessageHandler
  def initialize(message_builder_factory, parser_factory, saver_factory, tracker_finder_factory, raw_message)
    @message_builder_factory = message_builder_factory
    @parser_factory = parser_factory
    @saver_factory = saver_factory
    @tracker_finder_factory = tracker_finder_factory
    @raw_message = raw_message
  end

  def handle
    finder = @tracker_finder_factory.create_tracker_finder(@raw_message)
    tracker_model = finder.find_tracker
    imei = finder.imei

    parser = @parser_factory.create_parser(tracker_model)
    message = parser.parse(@raw_message)
    saver = @saver_factory.create_saver(message, imei)
    saver.save
  end
end
