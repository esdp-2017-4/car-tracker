require_relative 'parsers'
require_relative 'utils'

class ParserFactory
  def initialize
    @parser_map = {
        'coban_103' => Coban103Parser
    }
  end

  def create_parser(tracker_type)
    parser = @parser_map[tracker_type].new
    # parser.message_builder = @message_builder_factory.create_message_builder
    return parser
  end
end

class SaverFactory

  def create_saver(message, imei)
    saver = MessageSaver.new(message, imei)
  end

end


class MessageBuilder
  def add_type(type)
    @type = type
  end

  def add_sim(sim)
    @sim = sim
  end

  def add_longtitute(longtitute)
    @longtitute = longtitute
  end

  def add_latitude(latitude)
    @latitude = latitude
  end


  def add_time(time)
    @time = Time.at(time.to_i)
  end

  def add_tracker(tracker)
    @tracker = tracker
  end

  def build
    message = Message.new
    message.type = @type
    message.tracker = @tracker
    message.time = @time
    message.sim = @sim
    message.latitude = @latitude
    message.longtitute = @longtitute

    return message
  end
end

class MessageBuilderFactory
  def create_builder
    return MessageBuilder.new
  end
end


class TrackerFinderFactory

  def create_tracker_finder(message)
    return TrackerFinder.new(message)
  end

end


class MessageHandlerFactory

  def initialize(message_builder_factory,
                 parser_factory,
                 saver_factory,
                 tracker_finder_factory)

    @message_builder_factory = message_builder_factory
    @parser_factory = parser_factory
    @saver_factory = saver_factory
    @tracker_finder_factory = tracker_finder_factory

  end

  def create_message_handler(raw_message)
    return MessageHandler.new(
       @message_builder_factory,
       @parser_factory,
       @saver_factory,
       @tracker_finder_factory,
       raw_message)
  end

end